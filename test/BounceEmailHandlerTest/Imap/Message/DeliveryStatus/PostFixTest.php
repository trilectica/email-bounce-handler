<?php

namespace BounceEmailHandlerTest\Imap\Message\DeliveryStatus;

use BounceEmailHandler\Imap\Message\DeliveryStatus\Postfix;

class PostFixTest extends \PHPUnit_Framework_TestCase
{
    public function testGetDiagnosticCode()
    {
        $object = new Postfix(file_get_contents(__DIR__ . '/../emails/test550.txt'));
        $this->assertEquals('550', $object->getDiagnosticCode());

        $object = new Postfix(file_get_contents(__DIR__ . '/../emails/test552.txt'));
        $this->assertEquals('552', $object->getDiagnosticCode());

        $object = new Postfix(file_get_contents(__DIR__ . '/../emails/test554.txt'));
        $this->assertEquals('554', $object->getDiagnosticCode());

        $object = new Postfix(file_get_contents(__DIR__ . '/../emails/testnocode.txt'));
        $this->assertFalse($object->getDiagnosticCode());

        $object = new Postfix(file_get_contents(__DIR__ . '/../emails/test550_without_diag_code.txt'));
        $this->assertEquals(550, $object->getDiagnosticCode());

        $object = new Postfix(file_get_contents(__DIR__ . '/../emails/test550_without_diag_code2.txt'));
        $this->assertEquals(550, $object->getDiagnosticCode());

        $object = new Postfix(file_get_contents(__DIR__ . '/../emails/test550_without_diag_code_false.txt'));
        $this->assertFalse($object->getDiagnosticCode());
    }

    public function testGetOriginalRecipient()
    {
        $object = new Postfix(file_get_contents(__DIR__ . '/../emails/test550.txt'));
        $this->assertEquals('geen@geen.nl', $object->getOriginalRecipient());
    }

    public function testGetDiagnosticReason()
    {
        $object = new Postfix(file_get_contents(__DIR__ . '/../emails/test550.txt'));
        $this->assertEquals('Requested action not taken: mailbox unavailable', $object->getDiagnosticReason(550));

        $this->assertFalse($object->getDiagnosticReason('non existing'));
    }
}
