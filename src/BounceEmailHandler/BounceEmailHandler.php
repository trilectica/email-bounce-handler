<?php

namespace BounceEmailHandler;

use BounceEmailHandler\Imap\Connection;

class BounceEmailHandler
{
    /**
     * @var Connection $connection
     */
    protected $connection;

    /**
     * Fetch the messages from the provided mailbox.
     * Returns an error when the connection failed.
     *
     * @param Connection $connection
     *
     * @return \ArrayIterator
     *
     * @throws \Exception
     */
    public function process(Connection $connection)
    {
        return $connection->getMessages();
    }

    /**
     * @param $mailbox
     * @param $username
     * @param $password
     *
     * @return Connection
     */
    public function getConnection($mailbox, $username, $password)
    {
        if ($this->connection) {
            return $this->connection;
        }

        $this->connection = new Connection($mailbox, $username, $password);
        $this->connection->connect();

        return $this->connection;
    }
}
