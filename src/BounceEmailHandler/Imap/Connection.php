<?php

namespace BounceEmailHandler\Imap;

class Connection
{
    protected   $mailbox,
                $user,
                $password,
                $errors,
                $connection;

    public function __construct($mailbox, $user, $password)
    {
        $this->mailbox      = $mailbox;
        $this->user         = $user;
        $this->password     = $password;
    }

    public function connect()
    {
        $connection = imap_open($this->mailbox, $this->user, $this->password);
        if (! $connection) {
            $this->errors = imap_errors();

            return false;
        }

        $this->connection = $connection;

        return true;
    }

    public function getMessageCount()
    {
        if ($this->isConnected()) {
            return imap_num_msg($this->connection);
        }

        return false;
    }

    public function isConnected()
    {
        return $this->connection ? true : false;
    }

    public function getMessages()
    {
        if (!$this->isConnected()) {
            throw new \Exception('Not connected to any IMAP');
        }

        $count      = $this->getMessageCount();
        $messages   = array();

        while($count > 0) {
            $msg        = new Message($count, $this->connection);
            $messages[] = $msg;

            --$count;
        }

        return new \ArrayIterator($messages);
    }

    public function deleteMarkedMessages()
    {
        imap_expunge($this->connection);
    }
}
