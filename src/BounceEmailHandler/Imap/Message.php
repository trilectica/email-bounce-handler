<?php

namespace BounceEmailHandler\Imap;

use BounceEmailHandler\Imap\Message\DeliveryStatus;

class Message
{
    protected   $messageId,
                $connection,
                $header,
                $body;

    public function __construct($messageId, $connection)
    {
        $this->messageId  = $messageId;
        $this->connection = $connection;
        $this->header     = imap_headerinfo($connection, $messageId);
        $this->body       = imap_body($connection, $messageId);
    }

    /**
     * Returns the date when the imap message is received.
     *
     * @param string $format
     * @return bool|string
     */
    public function getDate($format = 'd-m-Y H:i')
    {
        return date($format, strtotime($this->header->date));
    }

    /**
     * Returns the raw headers from the imap message in stdClass format.
     *
     * @see http://php.net/manual/en/function.imap-headerinfo.php
     * @return stdClass
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Returns the raw body from the imap message in string format.
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    public function isDeliveryStatus()
    {
        return $this->getDeliveryStatus()
            ? true
            : false;
    }

    public function getDeliveryStatus()
    {
        $status =  new DeliveryStatus($this->body);
        return $status->getBody();
    }

    public function markForDeletion()
    {
        imap_delete($this->connection, $this->messageId);
    }
}
