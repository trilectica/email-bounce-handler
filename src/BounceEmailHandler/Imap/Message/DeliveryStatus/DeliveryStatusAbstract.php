<?php

namespace BounceEmailHandler\Imap\Message\DeliveryStatus;

abstract class DeliveryStatusAbstract
{
    /**
     * The body of the mail message.
     *
     * @var string
     */
    protected $body;

    /**
     * Default error codes for SMTP.
     *
     * @see http://www.greenend.org.uk/rjk/tech/smtpreplies.html
     * @see http://tools.ietf.org/html/rfc821#page-36
     *
     * @var array
     */
    protected $smtpResponseCodes = array(
        "200"    => "(nonstandard success response, see rfc876)",
        "211"    => "System status, or system help reply",
        "214"    => "Help message",
        "220"    => "<domain> Service ready",
        "221"    => "<domain> Service closing transmission channel",
        "250"    => "Requested mail action okay, completed",
        "251"    => "User not local; will forward to <forward-path>",
        "252"    => "Cannot VRFY user, but will accept message and attempt delivery",
        "354"    => "Start mail input; end with <CRLF>.<CRLF>",
        "421"    => "<domain> Service not available, closing transmission channel",
        "450"    => "Requested mail action not taken: mailbox unavailable",
        "451"    => "Requested action aborted: local error in processing",
        "452"    => "Requested action not taken: insufficient system storage",
        "500"    => "Syntax error, command unrecognised",
        "501"    => "Syntax error in parameters or arguments",
        "502"    => "Command not implemented",
        "503"    => "Bad sequence of commands",
        "504"    => "Command parameter not implemented",
        "521"    => "<domain> does not accept mail (see rfc1846)",
        "530"    => "Access denied",
        "550"    => "Requested action not taken: mailbox unavailable",
        "551"    => "User not local; please try <forward-path>",
        "552"    => "Requested mail action aborted: exceeded storage allocation",
        "553"    => "Requested action not taken: mailbox name not allowed",
        "554"    => "Transaction failed",
    );

    /**
     * If the response codes above are not found these codes might be used in order to find
     * a status code in the body and match this to the response code.
     *
     * @see http://www.coldnet.net/tech/SMTPCodes.htm
     * @see http://www.fots.nl/index.php/smtp-reply-codes/
     * @see http://blogs.technet.com/b/samdrey/archive/2013/12/09/bookmark-smtp-ndr-codes-technet.aspx
     *
     * @var array
     */
    protected $smtpStatusCodes = array(
        '4.0.0' => 552, //'Mailbox full',
        '4.2.2' => 552, //'Mailbox full',
        '5.0.0' => 552, //'Mailbox full',
        '5.1.0' => 500, //'Other address status',
        '5.1.1' => 550, //'Bad destination mailbox address',
        '4.1.1' => 550, //'Bad destination mailbox address',
        '5.1.2' => 550, //'Bad destination system address',
        '4.1.2' => 550, //'Bad destination system address',

        /** 5.1.x messages mean problem with email address. */
        '5.1.3' => 550, //'Bad destination mailbox address syntax',
        '5.1.4' => 550, //'Destination mailbox address ambiguous',
        '5.1.5' => 550, //'Destination mailbox address valid',
        '5.1.6' => 550, //'Mailbox has moved',
        '5.1.7' => 550, //'Bad sender\'s mailbox address syntax',
        '5.1.8' => 550, //'Bad sender\'s system address',

        /** 5.2.x NDR caused by a problem with the large size of the email. */
        '5.2.0' => 550, //'Other or undefined mailbox status',
        '5.2.1' => 550, //'Mailbox disabled, not accepting messages',
        '5.2.2' => 552, //'Mailbox full',
        '5.2.3' => 550, //'Message length exceeds administrative limit.',
        '5.2.4' => 550, //'Mailing list expansion problem',
        '5.3.0' => 550, //'Other or undefined mail system status',
        '5.3.1' => 550, //'Mail system full',
        '5.3.2' => 550, //'System not accepting network messages',
        '5.3.3' => 550, //'System not capable of selected features',
        '5.3.4' => 550, //'Message too big for system',
        '5.4.0' => 550, //'Other or undefined network or routing status',
        '5.4.1' => 550, //'No answer from host',
        '4.4.1' => 550, //'No answer from host',
        '5.4.2' => 550, //'Bad connection',
        '4.4.2' => 550, //'Bad connection',
        '5.4.3' => 550, //'Routing server failure',
        '5.4.4' => 550, //'Unable to route',
        '5.4.5' => 550, //'Network congestion',
        '5.4.6' => 550, //'Routing loop detected',
        '5.4.7' => 550, //'Delivery time expired',
        '5.5.0' => 550, //'Other or undefined protocol status',
        '5.5.1' => 550, //'Invalid command',
        '5.5.2' => 550, //'Syntax error',
        '5.5.3' => 550, //'Too many recipients',
        '5.5.4' => 550, //'Invalid command arguments',
        '5.5.5' => 550, //'Wrong protocol version',
        '5.6.0' => 550, //'Other or undefined media error',
        '5.6.1' => 550, //'Media not supported',
        '5.6.2' => 550, //'Conversion required and prohibited',
        '5.6.3' => 550, //'Conversion required but not supported',
        '5.6.4' => 550, //'Conversion with loss performed',
        '5.6.5' => 550, //'Conversion failed',
        '5.7.0' => 550, //'Other or undefined security status',
        '5.7.1' => 550, //'Delivery not authorized, message refused',
        '5.7.2' => 550, //'Mailing list expansion prohibited',
        '5.7.3' => 550, //'Security conversion required but not possible',
        '5.7.4' => 550, //'Security features not supported',
        '5.7.5' => 550, //'Cryptographic failure',
        '5.7.6' => 550, //'Cryptographic algorithm not supported',
        '5.7.7' => 550, //'Message integrity failure'
    );

    /**
     * Construct the Object.
     *
     * @param $body
     */
    abstract function __construct($body);

    /**
     * Retrieve the original recipient from the body.
     *
     * @return string
     */
    abstract function getOriginalRecipient();

    /**
     * Retrieve the diagnostic code from the body. The code returned should be a code
     * which is found in the smtpResponseCodes above.
     *
     * @return int
     */
    abstract function getDiagnosticCode();

    /**
     * Retrieve the code from a string. Loops through the smtpResponseCodes which
     * are known. If the code is found in the string the code is returned.
     *
     * If no code is found in the provided string, false will be returned.
     *
     * @param $string
     *
     * @return bool|int
     */
    protected function findCodeInString($string)
    {
        foreach ($this->smtpResponseCodes as $code => $description) {
            if (stripos($string, (string) $code) !== false) {
                return $code;
            }
        }

        foreach ($this->smtpStatusCodes as $code => $actualCode) {
            if (stripos($string, (string) $code) !== false) {
                return $actualCode;
            }
        }

        return false;
    }

    /**
     * Get the diagnostic reason for a specific code.
     * If the code is not present (known) false will be returned.
     *
     * @param $code
     *
     * @return bool|string
     */
    public function getDiagnosticReason($code)
    {
        return isset($this->smtpResponseCodes[$code])
            ? $this->smtpResponseCodes[$code]
            : false;
    }
}
