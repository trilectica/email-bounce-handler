<?php

namespace BounceEmailHandler\Imap\Message\DeliveryStatus;

class Postfix extends DeliveryStatusAbstract
{
    public function __construct($body)
    {
        $firstPartOfBody    = substr($body, 0, stripos($body, 'message/delivery-status'));
        $lastPartOfBody     = substr($body, stripos($body, 'message/delivery-status'));

        $firstHalf          = substr($firstPartOfBody, strrpos($firstPartOfBody, '--'));
        $lastHalf           = substr($lastPartOfBody, 0, stripos($lastPartOfBody, '--'));

        $this->body         = $firstHalf . $lastHalf;
    }

    protected function findCodeInString($string)
    {
        $parentReturn = parent::findCodeInString($string);
        if ($parentReturn !== false) {
            return $parentReturn;
        }

        return parent::findCodeInString($this->getStatusCode());
    }

    protected function getStatusCode()
    {
        $pattern = "/Status:[\s\S]*?\r\n/";
        preg_match($pattern, $this->body, $matches);

        $found = end($matches);

        $patternReplace = array("/(Status: )/", "(\r\n)");
        $foundFiltered = trim(preg_replace($patternReplace, '', $found));

        return $foundFiltered;
    }

    public function getDiagnosticCode()
    {
        $pattern = "/Diagnostic-Code:[\s\S]*?\r\n\r\n/";
        preg_match($pattern, $this->body, $matches);

        $found = end($matches);

        $patternReplace = array("/(Diagnostic-Code: )/", "(\r\n\r\n)", "(smtp;)");
        $foundFiltered = trim(preg_replace($patternReplace, '', $found));

        return $this->findCodeInString($foundFiltered);
    }

    public function getOriginalRecipient()
    {
        $pattern = "/Original-Recipient:[\s\S]*?\r\n/";
        preg_match($pattern, $this->body, $matches);

        $found = end($matches);

        $patternReplace = array("/(Original-Recipient: )/", "(\r\n)", "(rfc822;)");
        $foundFiltered = trim(preg_replace($patternReplace, '', $found));

        return $foundFiltered;
    }
}
