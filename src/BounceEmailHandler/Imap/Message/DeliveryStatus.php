<?php

namespace BounceEmailHandler\Imap\Message;

use BounceEmailHandler\Imap\Message\DeliveryStatus\Exim;
use BounceEmailHandler\Imap\Message\DeliveryStatus\Postfix;

class DeliveryStatus
{
    /**
     * @var DeliveryStatusAbstract
     */
    protected $body;

    public function __construct($body)
    {
        // postfix check
        if (stripos($body, 'message/delivery-status') !== false) {
            $this->body = new Postfix($body);
            return;
        }

        // exim check
        if (stripos($body, 'X-Failed-Recipients') !== false) {
            $this->body = new Exim($body);
            return;
        }

        // not a bounce mail as it looks
        $this->body = null;
    }

    public function getBody()
    {
        return $this->body;
    }
}
