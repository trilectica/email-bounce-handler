### Welcome to the *Bounce Email Handler* plugin!

### DEVELOPMENT GUIDELINES

Development should be performed with the PSR-2 guidelines

Code added is added in English

With new components / features documentations should be updated accordingly

New features, bug-fixes and all other changes require unit tests

### STATUS ###

At the moment only a postfix implementation is done. Other smtp types should be implemented (e.g. Exim).
